# [0.13.0](https://gitlab.com/testbysteven/npm-automated-releases/compare/v0.12.0...v0.13.0) (2023-10-10)


### Features

* Simplify FirstElement render method ([8fc31d8](https://gitlab.com/testbysteven/npm-automated-releases/commit/8fc31d84a19b08804511d4952a5ae896eb87d4d7))
* Simplify FirstElement rendering ([fdf9d96](https://gitlab.com/testbysteven/npm-automated-releases/commit/fdf9d9684c4bae950d167b1f9bdd9fa3ae8e1ead))

# [0.12.0](https://gitlab.com/testbysteven/npm-automated-releases/compare/v0.11.1...v0.12.0) (2023-10-10)


### Features

* Add version property to SecondElement class ([1fef200](https://gitlab.com/testbysteven/npm-automated-releases/commit/1fef200901c603e294dd9655740d4ca953f86478))

## [0.11.1](https://gitlab.com/testbysteven/npm-automated-releases/compare/v0.11.0...v0.11.1) (2023-10-10)

# [0.11.0](https://gitlab.com/testbysteven/npm-automated-releases/compare/v0.10.0...v0.11.0) (2023-10-10)


### Features

* Add npm ci and build commands, update main.ts and package-four/main.ts ([198c861](https://gitlab.com/testbysteven/npm-automated-releases/commit/198c86191b5103f3767b09c3443f9f37e117562a))

# [0.10.0](https://gitlab.com/testbysteven/npm-automated-releases/compare/v0.9.0...v0.10.0) (2023-10-10)


### Features

* **package.json:** add @semantic-release/npm dependency ([b330d85](https://gitlab.com/testbysteven/npm-automated-releases/commit/b330d852ae7e609dd2bacad39c494ae29eb7f6ed))

# [0.9.0](https://gitlab.com/testbysteven/npm-automated-releases/compare/v0.8.1...v0.9.0) (2023-10-10)


### Features

* Add LitElement component to package-one ([240ce52](https://gitlab.com/testbysteven/npm-automated-releases/commit/240ce528b4301df538a4e8446991523090df7624))

## [0.8.1](https://gitlab.com/testbysteven/npm-automated-releases/compare/v0.8.0...v0.8.1) (2023-10-10)

## 0.7.0 (2023-10-10)

* [skip-ci] Bump version to ([d2f44ea](https://gitlab.com/testbysteven/npm-automated-releases/commit/d2f44ea))
* [skip-ci] Update changelog 0.7.0 ([006a22c](https://gitlab.com/testbysteven/npm-automated-releases/commit/006a22c))
* feat!: Try to force a breaking change ([701c0c5](https://gitlab.com/testbysteven/npm-automated-releases/commit/701c0c5))
* chore: update git pull command in .gitlab-ci.yml ([17fac57](https://gitlab.com/testbysteven/npm-automated-releases/commit/17fac57))
* refactor(ci): refactor GitLab CI config ([be84a2d](https://gitlab.com/testbysteven/npm-automated-releases/commit/be84a2d))



## 0.7.0 (2023-10-10)

* chore: update git pull command in .gitlab-ci.yml ([17fac57](https://gitlab.com/testbysteven/npm-automated-releases/commit/17fac57))
* refactor(ci): refactor GitLab CI config ([be84a2d](https://gitlab.com/testbysteven/npm-automated-releases/commit/be84a2d))
* [skip-ci] Bump version to ([d2f44ea](https://gitlab.com/testbysteven/npm-automated-releases/commit/d2f44ea))



## 0.6.0 (2023-10-10)

No changes.

## 0.5.0 (2023-10-10)

No changes.

## 0.4.0 (2023-10-10)

No changes.

## 0.3.0 (2023-10-10)

No changes.

## 0.1.0 (2023-10-10)

No changes.

## 0.0.3 (2023-10-10)

No changes.

## 0.0.0-development (2023-10-10)

* feat: bump version and update changelog ([7fd97d1](https://gitlab.com/testbysteven/npm-automated-releases/commit/7fd97d1))
* feat: Update changelog ([d0b97c6](https://gitlab.com/testbysteven/npm-automated-releases/commit/d0b97c6))
* refactor: update gitlab-ci.yml ([2376248](https://gitlab.com/testbysteven/npm-automated-releases/commit/2376248))
* refactor(ci): improve commit message retrieval and version bump ([33226e5](https://gitlab.com/testbysteven/npm-automated-releases/commit/33226e5))
* chore: Add CI_COMMIT_BRANCH to update-changelog script ([0501c0d](https://gitlab.com/testbysteven/npm-automated-releases/commit/0501c0d))
* chore: add git status to version bump job ([6d206f2](https://gitlab.com/testbysteven/npm-automated-releases/commit/6d206f2))
* chore: Bump version to $NEW_PACKAGE_VERSION ([fdbde3b](https://gitlab.com/testbysteven/npm-automated-releases/commit/fdbde3b))
* chore: update .gitlab-ci.yml file ([8ae9a6d](https://gitlab.com/testbysteven/npm-automated-releases/commit/8ae9a6d))
* chore: Update .gitlab-ci.yml to use GL_TOKEN instead of CI_JOB_TOKEN ([a2eac0f](https://gitlab.com/testbysteven/npm-automated-releases/commit/a2eac0f))
* chore: update authentication token for API request ([45a3b79](https://gitlab.com/testbysteven/npm-automated-releases/commit/45a3b79))
* chore: update bump_version_and_update_changelog script ([20b7608](https://gitlab.com/testbysteven/npm-automated-releases/commit/20b7608))
* chore: update gitlab-ci.yml ([7294588](https://gitlab.com/testbysteven/npm-automated-releases/commit/7294588))
* chore: update gitlab-ci.yml ([4fad5b4](https://gitlab.com/testbysteven/npm-automated-releases/commit/4fad5b4))
* chore(ci): add node:18 image ([5201596](https://gitlab.com/testbysteven/npm-automated-releases/commit/5201596))
* chore(ci): update .gitlab-ci.yml ([2de5afa](https://gitlab.com/testbysteven/npm-automated-releases/commit/2de5afa))
* chore(ci): update .gitlab-ci.yml configuration ([59ee669](https://gitlab.com/testbysteven/npm-automated-releases/commit/59ee669))
* chore(ci): update .gitlab-ci.yml with new CI_GIT_TOKEN ([4c53172](https://gitlab.com/testbysteven/npm-automated-releases/commit/4c53172))
* chore(ci): update curl command for changelog request ([0ba7cb1](https://gitlab.com/testbysteven/npm-automated-releases/commit/0ba7cb1))
* fix: fix commit count and commit messages retrieval ([4e72e2d](https://gitlab.com/testbysteven/npm-automated-releases/commit/4e72e2d))
* fix: Fix missing closing parentheses in curl command ([8f1801f](https://gitlab.com/testbysteven/npm-automated-releases/commit/8f1801f))
* fix: Update version in package.json to v0.0.3 ([0c1ad0c](https://gitlab.com/testbysteven/npm-automated-releases/commit/0c1ad0c))
* fix: Update version number to v0.0.4 ([6a83afc](https://gitlab.com/testbysteven/npm-automated-releases/commit/6a83afc))
* ci: install jq package for bump_version_and_update_changelog ([53116fc](https://gitlab.com/testbysteven/npm-automated-releases/commit/53116fc))
* style: Use parentheses for command substitution ([07afab5](https://gitlab.com/testbysteven/npm-automated-releases/commit/07afab5))
