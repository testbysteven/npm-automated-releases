import { LitElement, html } from 'lit'

export class FirstElement extends LitElement {
  override render() {
    return html`
    <p>Simplify This element</p>
    `
  }
}
customElements.define('first-element', FirstElement)
