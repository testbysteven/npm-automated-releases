import { LitElement, html } from 'lit'

export class AnotherElement extends LitElement {
  static override properties = {
    version: {},
  }
  version: string

  constructor() {
    super()
    this.version = 'STARTING'
  }

  override render() {
    return html`
    <p>Welcome to the Lit tutorial!</p>
    <p>This is the ${this.version} code.</p>
    `
  }
}
customElements.define('another-element', AnotherElement)
